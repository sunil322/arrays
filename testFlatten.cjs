const flatten = require('./flatten.cjs');
const nestedArray = [1, [2], [[3]], [[[4]]]];

const result = flatten(nestedArray);

console.log(result);

const find = require('./find.cjs');
const items = [1, 2, 3, 4, 6, 5];

let elementToBeSearched=5;

function callback(element){
    return element === elementToBeSearched;
}

const result = find(items,callback);
console.log(result);
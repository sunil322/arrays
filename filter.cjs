function filter(elements,callback){
    let filteredArray = [];
    if(Array.isArray(elements)){
        if(typeof callback === 'function'){
            for(let index in elements){
                if(callback(elements[index],index,elements) === true){
                    filteredArray.push(elements[index]);
                }
            }
            return filteredArray;
        }else{
            return filteredArray;
        }
    }else{
        return filteredArray;
    }
}

module.exports = filter;
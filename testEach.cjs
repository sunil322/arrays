const each = require('./each.cjs');
const items = [1,2,3,4,5,5];

function callback(currValue,index){
    console.log(`Element ${currValue} present at ${index} index`)
}

each(items,callback);

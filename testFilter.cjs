const filter = require('./filter.cjs');
const items = [1, 2, 3, 4, 5, 5];

function callback(element,index,elements){
    return element%2 === 0;
}

const result = filter(items,callback);

console.log(result);
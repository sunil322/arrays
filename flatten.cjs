const nestedArray = [1, [2], [[3]], [[[4]]]];
let flatArray = [];
function flatten(elements,depth=1){
    if(Array.isArray(elements)){
    for(let element of elements){
        if(Array.isArray(element)){
            if(depth>=1){
                flatten(element,depth-1);
            }else{
                if(typeof element !== 'undefined'){
                    flatArray.push(element);
                }
            }
        }else{
            if(typeof element !=='undefined'){
                flatArray.push(element);
            }
        }
       }
       return flatArray;
   }else{
    return flatArray;
   }
}

module.exports = flatten;

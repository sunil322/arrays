function map(elements, callback) {
    let newArray = [];
    if(Array.isArray(elements)){
        if(typeof callback === 'function'){
            for(let index=0;index<elements.length;index++) {
                newArray.push(callback(elements[index],index,elements));
            }
            return newArray;
        }else{
            return newArray;
        }
    }else{
        return newArray;
    }
}
module.exports = map;

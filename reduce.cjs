function reduce(elements,callback,startingValue){
   if(Array.isArray(elements)){
    if(typeof callback === 'function'){
        let result;
        if(typeof startingValue !== 'undefined'){
            for(let index=0;index<elements.length;index++){
            result = callback(startingValue,elements[index],index,elements);
            startingValue = result;
            }
        }else{
            startingValue = elements[0];
            for(let index=1;index<elements.length;index++){
                result = callback(startingValue,elements[index],index,elements);
                startingValue = result;
            }
        }
    return result;
    }
   }
}

module.exports = reduce;
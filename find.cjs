function find(elements,callback){
    if(Array.isArray(elements)){
        if(typeof callback === 'function'){
            for(let element of elements){
                if(callback(element)){
                   return true;
                }
            }
        }
    }
}

module.exports =find;
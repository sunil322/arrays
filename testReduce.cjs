const reduce = require('./reduce.cjs');
const items = [1, 2, 3, 4, 5, 5];

function callback(startValue,currvalue,currentIndex,array){
    return startValue+array[currentIndex];
}

const result = reduce(items,callback,5);
console.log(result);
